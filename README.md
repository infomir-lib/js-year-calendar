# Internal repository
This is a fork of js-year-calendar with some modifications/bugfixes for internal use

# js-year-calendar
A fully customizable year calendar widget

## Requirements

This plugin uses pure javascript. No library is required.

## Installation

## Initialization

If you're using javascript modules, don't forget to import the library:

```
import Calendar from 'js-year-calendar';
import 'js-year-calendar/dist/js-year-calendar.css';
```

## Usage

You can create a calendar using the following javascript code :
```
new Calendar('.calendar')
```

Or

```
new Calendar(document.querySelector('.calendar'));
```

Where `.calendar` is the selector of a `DIV` element that should contain your calendar.

You can also use the following HTML if you don't want to use javascript to initialize the calendar
```
<div data-provide="calendar"></div>
```
The calendar will be automatically created when the page will finish loading

## Using options

You can specify options to customize the calendar:
```
new Calendar('.calendar', {
    style: 'background',
    minDate: new Date()
})
```

You can find the exhaustive list of options in the [documentation](https://year-calendar.github.io/js-year-calendar/documentation).

## Language

If you want to use the calendar in a different language, you should import the locale file corresponding to the language you want to use, and then set the `language` prop of the calendar:

```
import Calendar from 'js-year-calendar';
import 'js-year-calendar/locales/js-year-calendar.fr';
```

OR

```
<script src="https://unpkg.com/js-year-calendar@latest/dist/js-year-calendar.umd.min.js"></script>
<script src="https://unpkg.com/js-year-calendar@latest/locales/js-year-calendar.fr.js"></script>
```

Then

```
new Calendar('.calendar', {
    language: 'fr'
})
```

The list of available languages is available [here](https://github.com/year-calendar/js-year-calendar/tree/master/locales)

## Updating calendar

You can update the calendar after being instantiated:
```
const calendar = new Calendar('.calendar');

calendar.setStyle('background');
calendar.setMaxDate(new Date());
```

You can find the exhaustive list of methods in the [documentation](https://year-calendar.github.io/js-year-calendar/documentation).

## Events

You can bind events to the calendar at initialization
```
const calendar = new Calendar('.calendar', {
    clickDay: function(e) {
        alert('Click on day ' + e.date.toString());
    }
});
```

or later

```
new Calendar('.calendar');
document.querySelector('.calendar').addEventListener('clickDay', function(e) {
    alert('Click on day ' + e.date.toString());
});
```

You can find the exhaustive list of events in the [documentation](https://year-calendar.github.io/js-year-calendar/documentation).

